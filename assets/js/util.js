
// Validation functions

function validateCompanyModal(company) {
    if (isEmpty(company.name)) {
        showError("Nimi on kohustuslik!")
        return false;
    }
    if(!isEmpty(company.employees) && !isInteger(company.employees)) {
        showError("Töötajate arv peab olema täisarv!");
        return false;
    }
    if(!isEmpty(company.revenue) && !isDecimal(company.revenue)) {
        showError("Müügitulu peab olema number!");
        return false;
    }
    if(!isEmpty(company.netIncome) && !isDecimal(company.netIncome)) {
        showError("Netotulu peab olema number!");
        return false;
    }
    if(!isEmpty(company.securities) && !isInteger(company.securities)) {
        showError("Aktsiate arv peab olema täisarv!");
        return false;
    }
    if (!isEmpty(company.securityPrice) && !isDecimal(company.securityPrice)) {
        showError("Aktsiahind peab olema number!");
        return false;
    }
    if (!isEmpty(company.dividends) && !isDecimal(company.dividends)) {
        showError("Dididend peab olema number!");
        return false;
    }
    return true;
}

//pole vist kohe vaja
// function validateUserModal (username) {
//     if (isEmpty(username.username)) {
//         showError("Username is compulsory?")
//     }
//     if(!isEmpty(username) && !isInteger(company.employees)) {
//         showError("");
//         return false;
//     }
//     if(!isEmpty(company.revenue) && !isDecimal(company.revenue)) {
//         showError("Müügitulu peab olema number!");
//         return false;
//     }
//     if(!isEmpty(company.netIncome) && !isDecimal(company.netIncome)) {
//         showError("Netotulu peab olema number!");
//         return false;
//     }
//     if(!isEmpty(company.securities) && !isInteger(company.securities)) {
//         showError("Aktsiate arv peab olema täisarv!");
//         return false;
//     }
//     if (!isEmpty(company.securityPrice) && !isDecimal(company.securityPrice)) {
//         showError("Aktsiahind peab olema number!");
//         return false;
//     }
//     if (!isEmpty(company.dividends) && !isDecimal(company.dividends)) {
//         showError("Dididend peab olema number!");
//         return false;
// }

function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

function getCompanyFromModal() {
    return {
        "id": document.getElementById("id").value, 
        "name": document.getElementById("name").value,
        "logo": document.getElementById("logo").value,
        "established": document.getElementById("established").value,
        "employees": document.getElementById("employees").value,
        "revenue": document.getElementById("revenue").value,
        "netIncome": document.getElementById("netIncome").value,
        "securities": document.getElementById("securities").value,
        "securityPrice": document.getElementById("securityPrice").value,
        "dividends": document.getElementById("dividends").value
    };
}

function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

//for reading the registration input
function getCredentialsFromRegistrationContainer() {
    return {
        username: document.getElementById("uregUsername").value,
        password: document.getElementById("uregPassword").value,
        name: document.getElementById("uregName").value,
        age: document.getElementById("uregAge").value,
        gender: document.getElementById("uregGender").value,
        // picture: ,
        location: document.getElementById("uregLocation").value,
        description: document.getElementById("uregDescription").value
    }
}


function getCredentialsFromChangeRegistrationContainer() {
    return {
        username: document.getElementById("uregUsername").value,
        password: document.getElementById("uregPassword").value,
        name: document.getElementById("uregName").value,
        age: document.getElementById("uregAge").value,
        gender: document.getElementById("uregGender").value,
        // "picture": document.getElementById("file").value,
        location: document.getElementById("uregLocation").value,
        description: document.getElementById("uregDescription").value
    }

    }
//for reading the pet registration input (ownerId connection goes where tho?)

function getCredentialsFromPetRegistrationContainer() {
    return {
        name: document.getElementById("pregName").value,
        age: document.getElementById("pregAge").value,
        gender: document.getElementById("pregGender").value,
        petType: document.getElementById("pregPetType").value,
        breedId: document.getElementById("breedTypeLinks").value,
        breedSize: document.getElementById("pregBreedSize").value,
        inHeat: document.getElementById("pregInHeat").value,
        personality: document.getElementById("pregPersonality").value,
        description: document.getElementById("pregDescription").value,
        certification: document.getElementById("pregCertification").value
    }

}

//Registering swipe to database, credientials from DoggyTinder view
function getCredentialsFromSwipingLike() {
    return {
        from: currentPetId,
        like: swipingPetId
    }
}

function getCredentialsFromSwipingDislike() {
    return {
        from: currentPetId,
        dislike: swipingPetId
    }
}

// Financial functions

function formatNumber(num) {
    if (!isDecimal(num)) {
        return 0;
    }
    num = Math.round(num * 100) / 100;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// Chart Functions

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset(companies) {
    if (companies != null && companies.length > 0) {
        let data = companies.map(company => Math.round(company.marketCapitalization));
        let backgroundColors = companies.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabels(companies) {
    return companies != null && companies.length > 0 ? companies.map(company => company.name) : [];
}

function composeChartData(companies) {
    return {
        datasets: composeChartDataset(companies),
        labels: composeChartLabels(companies)
    };
}


function showDropDown() {
    document.getElementById("breedDropDown").style.display = "block";
    document.getElementById("pregBreedId").style.display = "block";
}

function generateOptions() {
    let breedTypeLinksElement = document.querySelector("#breedTypeLinks");
    let option = document.createElement("option");
    option.text = "";
    option.value = "";
    let links = "";
    for(i = 0; i < breedTypeList.length; i++) {
      

        // option = document.createElement("option");
        // option.text = breedTypeList[i].name;
        // option.value = breedTypeList[i].id;
        // breedTypeLinksElement.add(option);

          links += `
        <option value="${breedTypeList[i].id}" onclick=closeBreedDrowDown()>${breedTypeList[i].name}</option>
        `;
     
    }
    breedTypeLinksElement.innerHTML = links;
}

function changePageMain () {
    window.location.replace("main.html");
};

function changePageChat () {
    window.location.replace("chat.html");
}

function changePageProfile() {
    window.location.replace("profile.html");
    loadAccountInfo();
    loadPetInfo();
}

function changePageSwipe() {
    window.location.replace("doggyTinder.html")
}

function closeBreedDrowDown () {
    document.getElementById("breedTypeLinks").size = 1;
    document.getElementById("pregBreedId").style.display = "none";
    document.getElementById("space").style.display = "block";
    document.getElementById("space2").style.display = "block";
    // document.getElementById("breedTypeLinks").style.display = "none";

    
}

function filterBreedType() {
    let input, filter, ul, li, a, i;
    generateOptions();

    input = document.getElementById("pregBreedId");
    filter = input.value.toUpperCase();
    div = document.getElementById("breedDropDown");
    option = div.getElementsByTagName("option");
    let count = 0;

    for (i = 0; i < option.length; i++) {
      txtValue = option[i].textContent || option[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        option[i].style.display = "";
        count ++;
        // let test = document.getElementById("breedTypeLinks").options.length;
        let size = document.getElementById("breedTypeLinks").size = count;
        // height = document.getElementById("breedTypeLinks").height = "100px";
      } else {
        option[i].style.display = "none";
      }
    }
  }


  //app dogimage loading
//   $(document).ready(function() {
//     let image = $("<dogimage>");
//     let div = $("<div>")
//     image.load(function() {
//       div.css({
//         "width": this.width,
//         "height": this.height,
//         "background-image": "url(" + this.src + ")"
//       });
//       $("#container").append(div);
//     });
//     image.attr("src", "dogimage.jpg");
//   });

$(function() {

    causeRepaintsOn = $("h1, h2, h3, p");

    $(window).resize(function() {
        causeRepaintsOn.css("z-index", 1);
    });

});