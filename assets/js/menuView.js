
function generateTopMenu() {
    let logoutLink = "";
    let accountLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = /*html*/`${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold; font-size: 1vw">LOG OUT</a>`;
        // accountLink = /*html*/
    }

    document.getElementById("topMenuContainer").innerHTML = /*html*/`
        <div style="padding: 5px; color: #FFFDD0; font-style: italic; font-size: 1.5em">
            <div class="row">
                <div class="col-6">
                    <strong id="dTi" onclick=showMain()><h1>DoggyTinder</h1></strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal; font-size: 1.5vw">
                    ${logoutLink}
                </div>
            </div>
        </div>
    `;
}

function closePetRegistrationModal() {
    document.getElementById("petRegistrationModal").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function closeRegistrationModal() {
    document.getElementById("userRegistrationModal").style.display = "none";
}

function closeMatchModal() {
    document.getElementbyId
}

function showLoginContainer() {
    let loginContainer = document.getElementById("loginContainer");
    let mainContainer = document.getElementById("mainContainer");

    if(mainContainer) {
        mainContainer.style.display = "none";
    }
    // loginContainer.style.display = "block";
    showSignIn();
}

function showMainContainer() {
    let loginContainer = document.getElementById("loginContainer");
    let mainContainer = document.getElementById("mainContainer");
    if (loginContainer) {
        loginContainer.style.display = "none";
    } else if (mainContainer) {
        mainContainer.style.display = "block";
    }
}

function showAccountContainer() {
    document.getElementById("accountOverView").style.display = "block";
    
}

function changeView() {
    if(document.getElementById("viewSelect").value === "CHART") {
        showCompanyChart();
    } else {
        showCompanyList();
    }
}

function showCompanyList() {
    document.getElementById("companyList").style.display = "block";
    document.getElementById("companyChart").style.display = "none";
    loadCompanies();
}

function showCompanyChart() {
    document.getElementById("companyList").style.display = "none";
    document.getElementById("companyChart").style.display = "block";
    loadChart();
}

function showMain() {
    // document.getElementById("app-container").style.display = "block";
    if (getUsername() != null && getUsername() != '') {  //kui sisse logitud, siis viib DoggyTinder vajutades main. Kui pole, siis viib signin.html.
        window.location.href = "http://127.0.0.1:5500/main.html";
    } else {
        showSignIn();
    }
}

function showSignIn() {
    // document.getElementById("app-container").style.display = "block";
    window.location.href = "http://127.0.0.1:5500/signin.html";
}

function displayAlertOutOfPets(errorMessage) {
    let errorBox = document.querySelector('#alertOutOfPets');
    errorBox.style.display = 'block'; // by default on div element block
    errorBox.innerText = errorMessage;
}
