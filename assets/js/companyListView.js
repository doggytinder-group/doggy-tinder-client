


function generateCompanyList(companies) {
    let rows = "";
    for(let i = 0; i < companies.length; i++) {
        rows += `
            ${generateCompanyRowHeading(companies[i])}
            ${generateCompanyRowElement(
                'Asutatud:', companies[i].established != null ? companies[i].established : 'info puudub',
                'Töötajaid:', companies[i].employees > 0 ? formatNumber(companies[i].employees) : 'info puudub'
            )}
            ${generateCompanyRowElement(
                'Müügitulu (EUR):', companies[i].revenue > 0 ? formatNumber(companies[i].revenue) : 'info puudub',
                'Netotulu (EUR):', companies[i].netIncome !== 0 ? formatNumber(companies[i].netIncome) : 'info puudub'
            )}
            ${generateCompanyRowElement(
                'Turuväärtus (EUR):', companies[i].marketCapitalization > 0 ? formatNumber(Math.round(companies[i].marketCapitalization)) : 'info puudub',
                'Dividendimäär:', companies[i].dividendYield > 0 ? formatNumber(companies[i].dividendYield) : 'info puudub'
            )}
            ${generateCompanyRowButtons(companies[i])}
        `;
    }
    document.getElementById("companyList").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}
                <div class="row">
                    <div class="col-12" style="padding: 5px;">
                        <button class="btn btn-success btn-block" onClick="openCompanyModalForInsert()">Lisa ettevõte</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}

//hea kommentaar

function generateAppPetOverview (id) {
    swipingPetId = id.id;
    console.log(swipingPetId);
    fetchPet(getUsername()).then(updateCurrentPetId);
    document.getElementById("doginfo").innerHTML = /*html*/`${id.name}, ${id.age} <br>
    ${id.breedName}, ${id.gender} <br>${id.breedSize} and ${id.personality}`;

    document.getElementById("picture").innerHTML = /*html*/`<img src="${id.picture}" width: "auto"/>`;

    document.getElementById("description").innerHTML = /*html*/`${id.description} <br> ${id.certification}`;

    document.getElementById("ownerinfo").innerHTML = /*html*/`${id.owner.name}, ${id.owner.age}
    <br> ${id.owner.gender} <br>Location: ${id.owner.location}`;
}
function updateCurrentPetId(id) {
    currentPetId = id.id;
    console.log(currentPetId);
}

function generateUserOverview(username) {
    let rows = "";
    rows = `
       ${generateAccountRowHeading(username)}
       ${generateAccountRowElement(
        'Username:', username.username)} 
        ${generateAccountRowElement(
        'Age:', username.age != null ? username.age : 'Age not available.')}
        ${generateAccountRowElement(
        'Gender:', username.gender)}  
        ${generateAccountRowElement(
        'Location:', username.location != null ? username.location : 'Location not available.')}
        ${generateAccountRowElement(
        'Description:', username.description != null ? username.description : 'No description available.')}
        ${generateAccountRowButtons(username)}
        `;
    
    document.getElementById("accountOverview").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-8">
                ${rows}
                <div class="row">
                   
                </div>
            </div>
        </div>
    `;

    // <div class="col-12" style="padding: 5px;">
    // <button class="btn btn-success btn-block" onClick="openCompanyModalForInsert()">Lisa ettevõte</button>
    // </div>
}

function generatePetOverView(id) {
    // console.log(id.name);
    let rows = "";
    rows = `
       ${generatePetRowHeading(id)}
       ${generatePetRowElement(
        'Name:', id.name)} 
        ${generatePetRowElement(
        'Age:', id.age)}
        ${generatePetRowElement(
        'Gender:', id.gender)}  
        ${generatePetRowElement(
        'Breed size:', id.breedSize)}  
        ${generatePetRowElement(
        'Personality:', id.personality)}  
        ${generatePetRowElement(
        'Description:', id.description != null ? id.description : 'No description available.')}
        ${generatePetRowElement(
        'Certification:', id.certification)}  
        ${generatePetRowButtons(id)}
        `;
    
    document.getElementById("petOverview").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-8">
                ${rows}
            </div>
        </div>
    `;
}



   function generateAccountRowHeading(username) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 32px; color: white;" >Welcome, ${username.name}!</strong>
            </div>
        </div>
        // <div class="row">
        //     <div class="col-12" style="padding: 10px;">
        //         ${generateImageElement(username.picture)}
        //     </div>
        // </div>
    `;
}

function generatePetRowHeading(id) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 32px; color: white " >Your pet, ${id.name}</strong>
            </div>
        </div>
        // <div class="row">
        //     <div class="col-12" style="padding: 10px;">
        //         ${generateImageElement(id.picture)}
        //     </div>
        // </div>
    `;
}

function generatePetRowElement(cell1Content, cell2Content) {
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="row">
                    ${generatePetRowTitleCell(cell1Content)}
                    ${generatePetRowValueCell(cell2Content)}
                </div>
            </div>
        </div>
    `;
}

function generateAccountRowElement(cell1Content, cell2Content) {
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="row">
                    ${generateAccountRowTitleCell(cell1Content)}
                    ${generateAccountRowValueCell(cell2Content)}
                </div>
            </div>
        </div>
    `;
}


function generateCompanyRowHeading(company) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 28px;">${company.name}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                ${generateImageElement(company.logo)}
            </div>
        </div>
    `;
}

function generateCompanyRowElement(cell1Content, cell2Content, cell3Content, cell4Content) {
    return /*html*/`
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateCompanyRowTitleCell(cell1Content)}
                    ${generateCompanyRowValueCell(cell2Content)}
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    ${generateCompanyRowTitleCell(cell3Content)}
                    ${generateCompanyRowValueCell(cell4Content)}
                </div>
            </div>
        </div>
    `;
}





function generateCompanyRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateCompanyRowValueCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center; ">
                ${text}
            </div>
        </div>
    `;
}

function generatePetRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #FFFDD0; border-bottom: 1px solid #FFFDD0; border-radius: 10px; margin: 2px; padding: 10px; text-align: center; background-color: #fc8d83;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generatePetRowValueCell(text) {
    return /*html*/`
    <div class="col-sm-6" style="padding: 5px;">
        <div style="border-top: 1px solid #fc8d83; border-bottom: 1px solid #fc8d83; border-radius: 10px; margin: 2px; padding: 10px; text-align: center; background-color: #FFFDD0;">
            ${text}
        </div>
    </div>
`;
}

function generateAccountRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #FFFDD0; border-bottom: 1px solid #FFFDD0; border-radius: 10px; margin: 2px; padding: 10px; text-align: center; background-color: #fc8d83;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateAccountRowValueCell(text) {
    return /*html*/`
    <div class="col-sm-6" style="padding: 5px;">
        <div style="border-top: 1px solid #fc8d83; border-bottom: 1px solid #fc8d83; margin: 2px; padding: 10px; border-radius: 10px; text-align: center; background-color: #FFFDD0;">
            ${text}
        </div>
    </div>
`;
}

function generateCompanyRowButtons(company) {
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openCompanyModalForUpdate(${company.id})">Muuda</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeCompany(${company.id})">Kustuta</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}

function generateAccountRowButtons(username) {
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openUserModalForUpdate('${username.username}')">Change</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}

function generatePetRowButtons(id) {
    console.log("nimi on " + id.name);
    console.log("Koera id on " + id.id);
    console.log(getUsername());
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block"  onClick="openPetModalForUpdate('${getUsername()}')">Change</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;

    
}



function generateImageElement(userPicture) {
    if (!isEmpty(userPicture)) {
        return /*html*/`<img src="${userPicture}" width="150" />`;
    } else {
        return "[NO PICTURE AVAILABLE]";
    }
}

function openCompanyModal() {
    $("#companyModal").modal('show');
}

function openAppModal() {
    $("#app-container").modal('show');
}

function closeAppModal() {
    $("#app-container").modal('hide');
}

function closeCompanyModal() {
    $("#companyModal").modal('hide');
}


function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}

function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}

function clearCompanyModal() {
    document.getElementById("id").value = null;
    document.getElementById("name").value = null;
    document.getElementById("logo").value = null;
    document.getElementById("established").value = null;
    document.getElementById("employees").value = null;
    document.getElementById("revenue").value = null;
    document.getElementById("netIncome").value = null;
    document.getElementById("securities").value = null;
    document.getElementById("securityPrice").value = null;
    document.getElementById("dividends").value = null;
}

function fillCompanyModal(company) {
    document.getElementById("id").value = company.id;
    document.getElementById("name").value = company.name;
    document.getElementById("logo").value = company.logo;
    document.getElementById("established").value = company.established;
    document.getElementById("employees").value = company.employees;
    document.getElementById("revenue").value = company.revenue;
    document.getElementById("netIncome").value = company.netIncome;
    document.getElementById("securities").value = company.securities;
    document.getElementById("securityPrice").value = company.securityPrice;
    document.getElementById("dividends").value = company.dividends;
} 

function fillUserModal(username) {
    document.getElementById("uregUsername").value = username.username;
    document.getElementById("uregPassword").value = username.password;
    document.getElementById("uregName").value = username.name;
    document.getElementById("uregAge").value = username.age;
    document.getElementById("uregGender").value = username.gender;
    // document.getElementById("file").value = username.picture;
    document.getElementById("uregLocation").value = username.location;
    document.getElementById("uregDescription").value = username.description;
}

function clearUserModal() {
    document.getElementById("uregName").value  = null;
    console.log("kustatab väljad");
    document.getElementById("uregAge").value = null;
    document.getElementById("uregGender").value = null;
    // document.getElementById("file").value = null;
    document.getElementById("uregLocation").value  = null;
    document.getElementById("uregDescription").value = null;
}

function fillPetModal(id) {
    // console.log("nimi on " + document.getElementById("pregName").value);
    document.getElementById("pregName").value  = id.name;
    document.getElementById("pregAge").value  = id.age;
    document.getElementById("pregGender").value  = id.gender;
    // document.getElementById("pregPetType").value  = id.petType;
    // document.getElementById("breedTypeLinks").value  = id.breedId;
    document.getElementById("pregBreedSize").value  = id.breedSize;
    document.getElementById("pregPersonality").value  = id.personality;
    // document.getElementById("pregInHeat").value  = id.inHeat;
    document.getElementById("pregDescription").value  = id.description;
    document.getElementById("pregCertification").value  = id.certification;
}

function clearPetModal() {
    if (document.getElementById("pregName") == null) {
        console.log("väärtus on null");
    }
    // let name = document.getElementById("pregName").value;
    // console.log("väärtus on " + name);
    document.getElementById("pregName").value = null;
    document.getElementById("pregAge").value = null;
    document.getElementById("pregGender").value = null;
    // document.getElementById("pregPetType").value  = null;
    // document.getElementById("breedTypeLinks").value  = null;
    document.getElementById("pregBreedSize").value = null;
    document.getElementById("pregPersonality").value  = null;
    // document.getElementById("pregInHeat").value  = null;
    document.getElementById("pregDescription").value = null;
    document.getElementById("pregCertification").value = null;
}



function fillCompanyModalLogoField(logo) {
    document.getElementById("logo").value = logo;
}

function fillUserModalPictureField(picture) {
   document.getElementById("file").value = picture;
   console.log(document.getElementById("file").value);
}
