
// function fetchCompanies() {
//     return fetch(
//         `${API_URL}/companies`,
//         {
//             method: 'GET',
//             headers: {
//                 'Authorization': `Bearer ${getToken()}`
//             }
//         }
//     )
//     .then(checkResponse)
//     .then(companies => companies.json());
// }

async function fetchCompanies() {
    try{
        let response = await fetch(
            `${API_URL}/companies`,
            {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                }
            }
        )
        checkResponse(response);
        return await response.json();
        
    } catch (e) {
        return Promise.reject("Request failed: + e");
    }
    
}

async function postUser(user) {
    await fetch (`${API_URL}/users/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        });
}

async function postUserChange(user) {
    await fetch (`${API_URL}/users/register/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(user)
        });
}

async function postPetChange(pet) {
    await fetch (`${API_URL}/pets/registerPet/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`
        },
        body: JSON.stringify(pet)
    });
}

async function postPet(pet) {
    await fetch (`${API_URL}/pets/registerPet`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`
        },
        body: JSON.stringify(pet)
    });
}


async function getBreedTypeList() {
    try{
        let response = await fetch(
            `${API_URL}/pets/generateBreedTypeList`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        );
        return await response.json();
    } catch (e) {
        return Promise.reject("Request failed:" + e);
    }
}

function fetchCompany(id) {
    return fetch(
        `${API_URL}/companies/${id}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(company => company.json());
}

async function fetchUser(username) {
    try {
        let response = await fetch (
            `${API_URL}/users/${username}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            }
        }
        );
        checkResponse(response);
        return await response.json();
        console.log("siin2");

    } catch (e) {
        return Promise.reject("Request failed:" + e);
        
    }
}


async function fetchPet(username) {
    try {
        let response = await fetch (
            `${API_URL}/pets/${username}`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            }
        }
        );
        checkResponse(response);
        return await response.json();
    } catch (e) {
        return Promise.reject("Request failed:" + e);
    }
}

async function fetchMatches() {
    try {
        let response = await fetch (
            `${API_URL}/swipes/matches`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            }
        }
        );
        checkResponse(response);
        return await response.json();
    } catch (e) {
        return Promise.reject("Request failed:" + e);
    }
}



async function fetchAppPet() {
    try {
        let response = await fetch (
            `${API_URL}/swipes/swiping`,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            }
        }
        );
        checkResponse(response);
        return await response.json();
    } catch (e) {
        return Promise.reject("Request failed:" + e);
    }
}

async function postSwipe(swipe) {
    await fetch (`${API_URL}/swipes/registerSwipe`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`
        },
        body: JSON.stringify(swipe)
    });
}


function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);
    return fetch(
            `${API_URL}/files/upload`, 
            {
                method: 'POST',
                body: formData
            }
    )
    .then(checkResponse).then(response => response.json());
}

function postCompany(company) {
    return fetch(
        `${API_URL}/companies`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(company)
        }
    )
    .then(checkResponse);
}

// function postUser2(username) {
//     return fetch(
//         `${API_URL}/users/`, 
//         {
//             method: 'POST', 
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${getToken()}`
//             },
//             body: JSON.stringify(username)
//         }
//     )
//     .then(checkResponse);
// }

// function deleteCompany(id) {
//     return fetch(
//         `${API_URL}/companies/${id}`,
//         {
//             method: 'DELETE',
//             headers: {
//                 'Authorization': `Bearer ${getToken()}`
//             }
//         }
//     )
//     .then(checkResponse);
// }

async function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse)
    .then(session => session.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        closeCompanyModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    // loadAccountInfo();
    // showAccountContainer();
    return response;
}
