let breedTypeList = [];
let swipingPetId = 0;
let currentPetId = 0;

function loadCompanies() {
    fetchCompanies().then(generateCompanyList);
}

function loadAccountInfo() {
    if (getUsername() != null && getUsername() != '') {
        console.log("siin")
        fetchUser(getUsername()).then(generateUserOverview);
    }
}

function loadPetInfo () {
    if (getUsername() != null && getUsername() != '') {
        fetchPet(getUsername()).then(generatePetOverView);
}}

function getMatches() {
    let matches = fetchMatches();
        
}

async function loadAppPet() {
    if (getUsername() != null && getUsername() != '') {
        try {
            let pet = await fetchAppPet();
            generateAppPetOverview(pet);
        } catch (e) {
            console.log("Pet not found");
            openAlertOutOfPetsModal();
        }
    }
}
// function loadAppUser() {
//     if (getUsername() != null && getUsername() != '') {
//           fetchAppPet().then(generateAppUserOverview);
// }}


function openCompanyModalForUpdate(id) {
    clearError();
    fetchCompany(id).then(company => {
        openCompanyModal();
        clearCompanyModal();
        fillCompanyModal(company);
    });
}

function openUserModalForUpdate(username) {
    // clearError();
    // console.log("openUserModalForUpdate");
    fetchUser(username).then(username => {
        handleUserRegistrationModalOpening();
        clearUserModal();
        fillUserModal(username);
    });
}

function openPetModalForUpdate(username) {
    // clearError();
    fetchPet(username).then(id => {
        handlePetRegistrationModalOpening();
        clearPetModal();
        console.log(id.name);
        console.log("openpetmodalforupdate");
        fillPetModal(id);
    });
}

function openCompanyModalForInsert() {
    openCompanyModal();
    clearError();
    clearCompanyModal();
}

function openAppModalForInsert() {
    openAppModal();
    clearError();
    clearCompanyModal();
}

function saveFile() {
    let fileInput = getFileInput();
    return uploadFile(fileInput.files[0]);
    // .then(response => fillUserModalPictureField(response.url));
}


function saveCompany() {
    let company = getCompanyFromModal();
    if (validateCompanyModal(company)) {
        postCompany(company)
            .then(() => {
                closeCompanyModal();
                loadCompanies();
            });
    }
}
//save user
async function saveUser(event) {
    event.preventDefault();
    console.log("SIIN");
    let fileUploadResponse = await saveFile();
    
    let username = getCredentialsFromChangeRegistrationContainer();
    username.picture = fileUploadResponse.url;
    await postUserChange(username);
    closeUserRegistrationModal();
    loadAccountInfo();
}

function removeCompany(id) {
    if(confirm('Soovid ettevõtet kustutada?')) {
        deleteCompany(id).then(loadCompanies)
    }
}

function loadChart() {
    fetchCompanies().then(generateCompanyChart)
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            changePageMain();
            generateTopMenu();
            // loadCompanies();
            loadAccountInfo();  
            
        })
    }
}

async function likePet() {
    let swipe = getCredentialsFromSwipingLike();
    await postSwipe(swipe);
    loadAppPet();
}
async function dislikePet() {
    let swipe = getCredentialsFromSwipingDislike();
    await postSwipe(swipe);
    loadAppPet();
}



function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}

async function registerUser(event) {
    event.preventDefault();

    let fileUploadResponse = await saveFile();

    let user = getCredentialsFromRegistrationContainer();
    user.picture = fileUploadResponse.url;
    // // check later if user credientals are correct
    await postUser(user);
    
    closeRegistrationModal();
    let session = await login({ "username":  document.getElementById("uregUsername").value, "password":  document.getElementById("uregPassword").value });
    storeAuthentication(session);
    generateTopMenu();
    changePageMain();
    // openAppModal();
    // handlePetRegistrationModalOpening();
}

// async function updateUser() {
//     let user = getCredentialsFromChangeRegistrationContainer();
//     await postUser(user);
//     closeRegistrationModal();
// }

async function registerPet(event) {
    event.preventDefault();
    let fileUploadResponse = await saveFile();

    let pet = getCredentialsFromPetRegistrationContainer();
    pet.picture = fileUploadResponse.url;
    // check later if pet credentials are correct
    await postPet(pet);
    closePetRegistrationModal();
    showMain();
}

async function loadBreedTypes() {
    breedTypeList = await getBreedTypeList();
    filterBreedType();
}

function handleUserRegistrationModalOpening() {
    $('#userRegistrationModal').modal('show');
}

function closeUserRegistrationModal() {
    $('#userRegistrationModal').modal('hide');
}

function handlePetRegistrationModalOpening() {
    $('#petRegistrationModal').modal('show');
}

function closePetRegistrationModal() {
    $('#petRegistrationModal').modal('hide');
}

function openAlertOutOfPetsModal() {
    $('#alertOutOfPetsModal').modal('show');
    displayAlertOutOfPets("We are out of pets, please wait a little or start chatting with your matches!");
}